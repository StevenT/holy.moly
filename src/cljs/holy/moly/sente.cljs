(ns holy.moly.sente
  (:require  [taoensso.sente :as sente]
             [system.components.sente :refer [new-channel-socket-client]]
             [com.stuartsierra.component :as component]
             ;; Optional, for Transit encoding:
             [taoensso.sente.packers.transit :as sente-transit]
             [re-frame.core :as re-frame]
             [holy.moly.events :as events]))


;; Holy Moly events
(defmulti push-msg-handler (fn [[id _]] id)) ; Dispatch on event key which is 1st elem in vector

(defmethod push-msg-handler :holy.moly/test
  [[id data]]
  (println "RECEIVED" id data)
  (re-frame/dispatch [::events/chat data]))

(defmethod push-msg-handler :chsk/ws-ping
  [[_]]
  (println "Ping from server"))


;; Sente events
(defmulti event-handler :id) ; Dispatch on event-id

(defmethod event-handler :default ; Fallback
  [{:as event :keys [id]}]
  (println "Unhandled event: " id))

(defmethod event-handler :chsk/handshake
  [{:as event :keys [?data]}]
  (let [[?uid ?csrf-token ?handshake-data] ?data]
    (println "Channel socket handshake:" ?data)))

(defmethod event-handler :chsk/state
  [{[_old-state-map new-state-map] :?data :as event}]
  (if (:first-open? new-state-map)
    (println "Channel socket successfully established!")
    (println "Channel socket state change: %s" (pr-str new-state-map))))

(defmethod event-handler :chsk/recv
  [{:as event :keys [?data]}]
  ;; (println "got event" event)
  (push-msg-handler ?data))


;; Init Sente
(def ?csrf-token
  (when-let [el (.getElementById js/document "sente-csrf-token")]
    (.getAttribute el "data-csrf-token")))

(def sente-client (component/start
                   (new-channel-socket-client event-handler
                                              "/chsk"
                                              ?csrf-token
                                              {:type :auto
                                               :packer (sente-transit/get-transit-packer) ;; :edn
                                               })))
(def chsk       (:chsk sente-client))
(def chsk-send! (:chsk-send! sente-client))
(def chsk-state (:chsk-state sente-client))

(re-frame/reg-fx
 :srv-send
 (fn [[event msg]]
   (println "SENDING" event msg)
   (chsk-send! [event msg])))
