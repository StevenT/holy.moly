(ns holy.moly.routes
  (:require [bidi.bidi :as bidi]
            [pushy.core :as pushy]
            [re-frame.core :as re-frame]
            [holy.moly.events :as events]))

(def routes ["/" {""      :home
                  "about" :about
                  true    :not-found}])

(defn- parse-url [url]
  (bidi/match-route routes url))

(defn- dispatch-route [matched-route]
  (let [panel (:handler matched-route)]
    (re-frame/dispatch [::events/set-active-panel panel])))

(defn app-routes []
  (pushy/start! (pushy/pushy dispatch-route parse-url)))

(def url-for (partial bidi/path-for routes))
