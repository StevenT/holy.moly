(ns holy.moly.views
  (:require
   [reagent.core :as r]
   [re-frame.core :as re-frame]
   [holy.moly.routes :as routes]
   [holy.moly.events :as events]
   [holy.moly.subs :as subs]))

(defn page [content]
  [:div
   [:nav.navbar.navbar-expand.navbar-dark.bg-dark
    [:div.container
     [:a.navbar-brand {:href "/"} [:img {:src "/assets/1024.png" :style {:height "1em"}}]]
     [:button.navbar-toggler {:type :button}
      [:span.navbar-toggler-icon]]
     [:div.collapse.navbar-collapse
      [:ul.navbar-nav.mr-auto
       [:li.nav-item [:a.nav-link {:href "/"} "Home"]]
       [:li.nav-item [:a.nav-link {:href "/about"} "About"]]
       [:li.nav-item [:a.nav-link {:href "/wqfwquq"} "404"]]]]]]
   [:div.container content]])

;; home
(defn home-panel []
  (let [val (r/atom "")
        chat (re-frame/subscribe [::subs/chat])]
    (fn []
      [page
       [:div
        [:div {:style {:height 600 :overflow :auto :border "1px solid red"}}
         (for [msg @chat]
           ^{:key msg}
           [:div msg])]
        [:form {:method :post
                :on-submit (fn [evt]
                             (.preventDefault evt)
                             (do (re-frame/dispatch [::events/srv-send @val])
                                 (reset! val "")))}
         [:input {:value @val
                  :on-change (fn [evt] (reset! val (-> evt .-target .-value)))}]
         [:button {:type :submit} "send"]]
        [:img {:src "/assets/1024.png" :style {:width 30}}]]])))

;; about
(defn about-panel []
  [page
   [:div "This is the About Page."]])

;; not found
(defn not-found-panel []
  [page
   [:div "404 - Page not found"]])

;; main

(defn- panels [panel-name]
  (case panel-name
    :home      [home-panel]
    :about     [about-panel]
    :not-found [not-found-panel]
    [:div]))

(defn show-panel [panel-name]
  [panels panel-name])

(defn main-panel []
  (let [active-panel (re-frame/subscribe [::subs/active-panel])]
    [show-panel @active-panel]))

