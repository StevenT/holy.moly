(ns holy.moly.client
  (:require [reagent.core :as reagent]
            [re-frame.core :as re-frame]
            [holy.moly.config :as config]
            [holy.moly.events :as events]
            [holy.moly.routes :as routes]
            [holy.moly.views :as views]
            [holy.moly.sente :as sente]))

(defn dev-setup []
  (when config/debug?
    (enable-console-print!)
    (println "Running app in development mode")))

(defn mount-root []
  (re-frame/clear-subscription-cache!)
  (reagent/render [views/main-panel]
                  (.getElementById js/document "app")))

(defn reload []
  (mount-root))

(defn ^:export init []
  (routes/app-routes)
  (re-frame/dispatch-sync [::events/initialize-db])
  (dev-setup)
  (mount-root))

