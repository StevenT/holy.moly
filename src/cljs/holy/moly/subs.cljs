(ns holy.moly.subs
  (:require [re-frame.core :as re-frame]))

(re-frame/reg-sub
 ::chat
 (fn [db]
   (:chat db)))

(re-frame/reg-sub
 ::active-panel
 (fn [db _]
   (:active-panel db)))
