(ns holy.moly.events
  (:require [re-frame.core :as re-frame]
            [holy.moly.db :as db]))

(re-frame/reg-event-db
 ::initialize-db
 (fn  [_ _]
   db/default-db))

(re-frame/reg-event-db
 ::set-active-panel
 (fn [db [_ active-panel]]
   (assoc db :active-panel active-panel)))

(re-frame/reg-event-fx
 ::srv-send
 (fn [{} [_ msg]]
   {:srv-send [:holy.moly/test msg]}))

(re-frame/reg-event-db
 ::chat
 (fn [db [_ msg]]
   (let [old (:chat db)]
     (assoc db :chat (conj old msg)))))


