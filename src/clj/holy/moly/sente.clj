(ns holy.moly.sente
  (:require [compojure.core :refer [routes GET POST ANY]]
            [taoensso.timbre :as timbre :refer [tracef debugf infof warnf errorf]]
            [taoensso.sente.server-adapters.http-kit :refer (get-sch-adapter)]
            [taoensso.sente :as sente]))

;; (timbre/set-level! :trace)    ; Uncomment for more logging
(reset! sente/debug-mode?_ true) ; Uncomment for extra debug info


;; Holy Moly events
(defmulti event-handler :id); Dispatch on event-id

(defmethod event-handler :default ; Fallback
  [{:as event :keys [id]}]
  (println "Unhandled event: " id))

(defmethod event-handler :holy.moly/test
  [{:as event :keys [id ?data client-id send-fn connected-uids]}]
  (println "RECEIVED" id "from client" client-id ?data)
  ;; (println "connected-uids" @connected-uids)
  ;; (send-fn client-id [:holy.moly/test "check!"])
  (let [now (java.util.Date.)
        time (.format (java.text.SimpleDateFormat. "HH:mm:ss") now)]
    (doseq [uid (:any @connected-uids)]
      (println "SENDING TO" uid)
      (send-fn uid [:holy.moly/test (str time ": " ?data)]))))

(defmethod event-handler :chsk/ws-ping
  [_]
  (println "ping from client"))

(defmethod event-handler :chsk/uidport-open
  [_]
  (println "uid port open"))


(defn sente-handler [{db :db}]
  (fn [{:keys [ring-req] :as event}]
    (let [session (:session ring-req)
          headers (:headers ring-req)
          ;; _ (println (keys event))
          event (select-keys event [:id :?data :client-id :uid :?reply-fn :send-fn :connected-uids])]
      ;; (println "received event" event)
      (event-handler event))))

