(ns holy.moly.core
  (:gen-class)
  (:require [system.repl :refer [set-init! start]]
            [holy.moly.systems :refer [prod-system]]))

(defn -main
  "Start the application"
  []
  (set-init! #'prod-system)
  (start))
