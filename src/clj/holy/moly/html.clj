(ns holy.moly.html
  (:require
   [ring.middleware.anti-forgery :as anti-forgery]
   (hiccup [page :refer [html5 include-js include-css]])))

(defn index []
  (let [csrf-token
          ;; (:anti-forgery-token ring-req) ; Also an option
        (force anti-forgery/*anti-forgery-token*)]
    (html5
     [:head
      [:meta {:charset "utf-8"}]
      [:meta {:http-equiv "X-UA-Compatible" :content "IE=edge"}]
      [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
      [:meta {:name "description" :content "Holy Moly!"}]
      [:title "Holy Moly!"]
      (include-css "//maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
                   "/css/main.css")]
     [:body
      [:div#sente-csrf-token {:data-csrf-token csrf-token}]
      [:div#app]
      [:noscript "This application needs JavaScript enabled!"]
      (include-js "/js/main.js")])))
