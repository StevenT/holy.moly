(ns holy.moly.routes
  (:require [compojure.core :refer [routes GET POST ANY]]
            [ring.util.http-response :as response]
            [holy.moly.html :as html]))

(defn ok-response [response]
  (-> (response/ok response)
      (response/header "Content-Type" "application/json; charset=utf-8")))

(defn api-routes [{db :db}]
  (routes
   (GET "/hello" [] (ok-response {:msg "Hello world!!"}))))

(defn ring-routes [_]
  (routes
   (GET "/" [] (html/index))
   (ANY "*" [] "404 - NOT FOUND!")))

