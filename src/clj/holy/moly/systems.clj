(ns holy.moly.systems
  (:require [com.stuartsierra.component :as component]
            [environ.core :refer [env]]
            [holy.moly.routes :refer [api-routes ring-routes]]
            [holy.moly.sente :refer [sente-handler]]
            [ring.middleware.format :refer [wrap-restful-format]]
            [ring.middleware.defaults :refer [wrap-defaults
                                              site-defaults
                                              api-defaults]]
            [ring.middleware.params :refer [wrap-params]]
            [ring.middleware.keyword-params :refer [wrap-keyword-params]]
            [ring.middleware.resource :refer [wrap-resource]]
            [ring.middleware.not-modified :refer [wrap-not-modified]]
            [ring.middleware.content-type :refer [wrap-content-type]]
            [taoensso.sente :as sente]
            [taoensso.sente.server-adapters.http-kit :refer [get-sch-adapter]]
            ;; Optional, for Transit encoding:
            [taoensso.sente.packers.transit :as sente-transit]

            (system.components
             [http-kit :refer [new-web-server]]
             [endpoint :refer [new-endpoint]]
             [middleware :refer [new-middleware]]
             [repl-server :refer [new-repl-server]]
             ;; [postgres :refer [new-postgres-database]]
             [handler :refer [new-handler]]
             [sente :refer [new-channel-socket-server sente-routes]])))

(def rest-middleware
  (fn [handler]
    (wrap-restful-format handler
                         :formats [:json-kw]
                         :response-options {:json-kw {:pretty true}})))

(defn dev-system []
  (component/system-map
   :sente (new-channel-socket-server sente-handler
                                     (get-sch-adapter)
                                     {:wrap-component? true
                                      :packer (sente-transit/get-transit-packer) ;; :edn
                                      :user-id-fn (fn [ring-req] (get-in ring-req [:params :client-id]))
                                      })

   :middleware (new-middleware {:middleware [[wrap-defaults site-defaults]]})
   :api-middleware (new-middleware {:middleware [rest-middleware [wrap-defaults api-defaults]]})

   :sente-endpoints (component/using
                     (new-endpoint sente-routes)
                     [:sente])
   :api-endpoints (component/using
                   (new-endpoint api-routes)
                   [:api-middleware])
   :site-endpoints (new-endpoint ring-routes)

   :handler (component/using
             (new-handler)
             [:api-endpoints :sente-endpoints :site-endpoints :middleware])
   :web-server (component/using
                (new-web-server (Integer. (env :http-port)))
                [:handler])))

(defn prod-system []
  (merge (dev-system)
         (component/system-map
          :repl-server (new-repl-server (read-string (env :repl-port))))))
