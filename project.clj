(defproject
  boot-project
  "0.0.0-SNAPSHOT"
  :repositories
  [["clojars" {:url "https://repo.clojars.org/"}]
   ["maven-central" {:url "https://repo1.maven.org/maven2"}]]
  :dependencies
  [[org.clojure/clojure "1.10.1"]
   [org.clojure/clojurescript "1.10.520"]
   [adzerk/boot-cljs "2.1.5" :scope "test"]
   [adzerk/boot-cljs-repl "0.4.0" :scope "test"]
   [adzerk/boot-reload "0.6.0" :scope "test"]
   [adzerk/boot-test "1.2.0" :scope "test"]
   [binaryage/devtools "0.9.10" :scope "test"]
   [cider/piggieback "0.4.1" :scope "test"]
   [nrepl "0.6.0" :scope "test"]
   [onetom/boot-lein-generate "0.1.3" :scope "test"]
   [powerlaces/boot-cljs-devtools "0.2.0" :scope "test"]
   [weasel "0.7.0" :scope "test"]
   [boot-environ "1.1.0"]
   [compojure "1.6.1"]
   [environ "1.1.0"]
   [hiccup "1.0.5"]
   [http-kit "2.3.0"]
   [metosin/ring-http-response "0.9.1"]
   [org.clojure/java.jdbc "0.7.9"]
   [org.clojure/tools.cli "0.4.2"]
   [org.clojure/tools.logging "0.4.1"]
   [org.danielsz/system "0.4.3"]
   [ring "1.7.1"]
   [ring-middleware-format "0.7.4"]
   [ring/ring-defaults "0.3.2"]
   [org.clojure/core.async "0.4.500"]
   [com.taoensso/sente "1.14.0-RC2"]
   [com.taoensso/timbre "4.10.0"]
   [com.cognitect/transit-clj "0.8.313"]
   [com.cognitect/transit-cljs "0.8.256"]
   [deraen/boot-sass "0.3.1"]
   [reagent "0.8.1"]
   [re-frame "0.10.7"]
   [bidi "2.1.6"]
   [kibu/pushy "0.3.8"]]
  :source-paths
  []
  :resource-paths
  ["src/sass" "src/cljs" "src/cljc" "src/clj" "resources"])