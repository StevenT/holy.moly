(def project 'holy.moly)
(def version "0.1.0-SNAPSHOT")

(set-env! :resource-paths #{"src/cljc" "src/cljs" "src/clj" "src/sass"  "resources"}
          :dependencies   '[[org.clojure/clojure "1.10.1"]
                            [org.clojure/clojurescript "1.10.520"]

                            ;; Dev
                            [adzerk/boot-cljs "2.1.5" :scope "test"]
                            [adzerk/boot-cljs-repl "0.4.0" :scope "test"]
                            [adzerk/boot-reload "0.6.0" :scope "test"]
                            [adzerk/boot-test "1.2.0" :scope "test"]
                            [binaryage/devtools "0.9.10" :scope "test"]
                            [cider/piggieback "0.4.1" :scope "test"]
                            [nrepl "0.6.0" :scope "test"]
                            [onetom/boot-lein-generate "0.1.3" :scope "test"]
                            [powerlaces/boot-cljs-devtools "0.2.0" :scope "test"]
                            [weasel "0.7.0" :scope "test"]

                            ;; Backend
                            [boot-environ "1.1.0"]
                            [compojure "1.6.1"]
                            [environ "1.1.0"]
                            [hiccup "1.0.5"]
                            [http-kit "2.3.0"]
                            [metosin/ring-http-response "0.9.1"]
                            [org.clojure/java.jdbc "0.7.9"]
                            [org.clojure/tools.cli "0.4.2"]
                            [org.clojure/tools.logging "0.4.1"]
                            [org.danielsz/system "0.4.3"]
                            [ring "1.7.1"]
                            [ring-middleware-format "0.7.4"]
                            [ring/ring-defaults "0.3.2"]

                            ;; Sente
                            [org.clojure/core.async "0.4.500"]
                            [com.taoensso/sente        "1.14.0-RC2"]
                            [com.taoensso/timbre       "4.10.0"]
                            ;; Transit deps optional; may be used to aid perf. of larger data payloads
                            ;; (see reference example for details):
                            [com.cognitect/transit-clj  "0.8.313"]
                            [com.cognitect/transit-cljs "0.8.256"]

                            ;; Frontend
                            [deraen/boot-sass  "0.3.1"]
                            [reagent "0.8.1"]
                            [re-frame "0.10.7"]
                            [bidi "2.1.6"]
                            [kibu/pushy "0.3.8"]
                            ])

(require '[system.boot :refer [system run]]
         '[holy.moly.systems :refer [dev-system]]
         '[clojure.edn :as edn]
         '[environ.core :refer [env]]
         '[environ.boot :refer [environ]]
         '[powerlaces.boot-cljs-devtools :refer [cljs-devtools]]
         '[boot.lein])

(require '[adzerk.boot-cljs :refer [cljs]]
         '[adzerk.boot-cljs-repl :refer [cljs-repl start-repl]]
         '[adzerk.boot-reload :refer [reload]]
         '[deraen.boot-sass :refer [sass]])

(boot.lein/generate)

(task-options!
 aot {:namespace   #{'holy.moly.core}}
 jar {:main        'holy.moly.core
      :file        (str "holy.moly-" version "-standalone.jar")})

(deftask dev
  "run a restartable system"
  []

  (task-options!
   cljs {:optimizations    :none
         :compiler-options {:parallel-build  true
                            :optimizations   :none
                            :external-config {:devtools/config
                                              {:features-to-install [:formatters :hints]
                                               :fn-symbol           "λ"}}}})
  (comp
   (environ :env {:http-port "8080"})
   (watch :verbose true)
   (system :sys #'dev-system
           :auto true
           :mode :lisp
           :files ["html.clj"
                   "routes.clj"
                   "sente.clj"
                   "systems.clj"])
   (repl :server true
         :host "127.0.0.1")
   (reload :asset-path "public"
           :on-jsload 'holy.moly.client/reload)
   (cljs-devtools)
   (cljs-repl)
   (cljs)
   (sass)
   (sift :move {#"main.css" "public/css/main.css"})
   (notify)))

(deftask build
  "Build the project locally as a JAR."
  [d dir PATH #{str} "the set of directories to write to (target)."]
  (let [dir (if (seq dir) dir #{"target"})]
    (comp (aot) (pom) (uber) (jar) (target :dir dir))))

(deftask run-project
  "Run the project."
  [a args ARG [str] "the arguments for the application."]
  (require '[kongauth.core :as app])
  (apply (resolve 'client/-main) args))

(require '[adzerk.boot-test :refer [test]])
